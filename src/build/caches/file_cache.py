# -*- coding: utf-8 -*-

import os,sys,math,logging
from .cache_json import CacheJson
from ...utils import py

'''
	文件缓存
'''
class FileCache(CacheJson):
	
	# 更新文件
	def updateFiles(self, files):
		filesData = self.getPathCache('files', {})
		filesData.clear()
		for f in files:
			t = math.floor(os.stat(f).st_mtime) 
			filesData[f] = t

	# 检查文件变动
	def checkFileChange(self, files):
		filesData = self.getPathCache('files', {})
		if len(files) != len(filesData):
			return True
		for f in files:
			t = math.floor(os.stat(f).st_mtime) 
			if filesData.get(f, 0) != t:
				return True
