# -*- coding: utf-8 -*-

import os
from ...utils import py

'''
	缓存JSON文件
'''
class CacheJson:

	__file = None
	__cache = None

	# 构造
	def __init__(self, file):
		self.__file = file

	# 加载
	def load(self):
		if os.path.exists(self.__file):
			self.__cache = py.readJson(self.__file)
		else:
			self.__cache = {}
	
	# 保存
	def save(self):
		if self.__cache and self.__file:
			chacheDir = os.path.dirname(self.__file)
			if not os.path.exists(chacheDir):
				os.makedirs(chacheDir)
			py.writeJson(self.__cache, self.__file, True)

	# 获得文件
	def getFile(self):
		return self.__file

	# 获得缓存对象
	def getCache(self):
		return self.__cache

	# 获得指定路径的缓存
	def getPathCache(self, path, defval, create = True):
		result = self.__cache
		parts = path.split(".")
		for i in range(0, len(parts)):
			p = parts[i]
			if p in result:
				result = result[p]
			else:
				if not create:
					return None
				t = defval if i == len(parts) - 1 else {}
				result[p] = t
				result = t
		return result
		