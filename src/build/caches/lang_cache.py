# -*- coding: utf-8 -*-

import os,logging
from .cache_json import CacheJson

'''
	字体索引缓存
'''
class FontIndexCache(CacheJson):
	
	# 添加字体
	def addFont(self, fontName, fontConfig, pack):
		cacheData = self.getPathCache('cache.font', {})
		if fontName in cacheData:
			logging.error('fontName `%s` conflict', fontName)
			return False
		cacheData[fontName] = 'asset://%s/lcc-assets/langs/%s' % (pack, fontName.replace('.','_'))
		fontData = self.getPathCache('fonts.' + fontName, {})
		if fontName in fontData:
			logging.error('fontName `%s` conflict', fontName)
			return False
		fontData.update(fontConfig)

	# 添加字体文本
	def addFontText(self, fontName, text):
		fontData = self.getPathCache('fonts.' + fontName, {})
		fontData['texts'] = fontData.get('texts', '') + text
	
	# 设置源文件路径
	def setSourcePath(self, sourcePath):
		self.getCache()['source'] = sourcePath

'''
	语言缓存
'''
class LangCache(CacheJson):
	
	# 添加语言文本
	def addLangText(self, lang, key, text, config, fontName = None, fontSize = None):
		if config.get('prepose',False):
			textData = self.getPathCache('prelangs.' + lang + '.texts', {})
		else:
			textData = self.getPathCache('cache.' + lang + '.texts', {})
		if key in textData:
			logging.error('LangCache.addLangText conflict key `%s`', key)
			return False
		item = { 'text' : text }
		if fontName != None:
			item['fontName'] = fontName
		if fontSize != None:
			item['fontSize'] = int(fontSize)
		textData[key] = item
		langData = self.getPathCache('langs.' + lang + '.texts', {})
		langData[key] = config

	# 查找语言文本
	def findLangText(self, lang, text, fontName = None, fontSize = None):
		textData = self.getPathCache('cache.' + lang + '.texts', {}, False)
		if textData:
			for (k,c) in textData.items():
				if c.get('text') == text and c.get('fontName') == fontName and c.get('fontSize') == fontSize:
					return k
		textData = self.getPathCache('prelangs.' + lang + '.texts', {}, False)
		if textData:
			for (k,c) in textData.items():
				if c.get('text') == text and c.get('fontName') == fontName and c.get('fontSize') == fontSize:
					return k

	# 添加语言图片
	def addLangImage(self, lang, key, image, config):
		if config.get('prepose',False):
			imageData = self.getPathCache('prelangs.' + lang + '.images', {})
		else:
			imageData = self.getPathCache('cache.' + lang + '.images', {})
		if key in imageData:
			logging.error('LangCache.addLangImage conflict key `%s`', key)
			return False
		imageData[key] = image
		langData = self.getPathCache('langs.' + lang + '.images', {})
		langData[key] = image
