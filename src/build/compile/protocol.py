# -*- coding: utf-8 -*-

import os,logging
from ... import utils
from ...utils import py

# 模板 - JS文件
TEMPLATE_JS_FILE = '''
/**
* generate by lcc-framework
*/
var protocol = globalThis.lcc.protocol;
if(!protocol){{
	protocol = {{}};
	globalThis.lcc.protocol = protocol;
}}

{commands}
{enums}
'''

# 模板 - 声明文件
TEMPLATE_DECLARE_FILE ='''
/**
* generate by lcc-framework
*/
declare namespace lcc {{

export namespace protocol {{

{commands}
{messages}

}}

}}
'''

# 模板 - 协议文件
TEMPLATE_PROTO_FILE = '''
// generate by lcc-framework

syntax = "proto3";

package {packName};

{messages}
'''

# 模板 - 协议消息
TEMPLATE_PROTO_MESSAGE = '''
message {messageName} {{
	{messageFields}
}}'''

# 模板 - JS枚举
TEMPLATE_JS_ENUM = '''
var enumMap = protocol.{enumName};
if(!enumMap){{
	enumMap = {{}};
	protocol.{enumName} = enumMap;
}}
{enumFields}
'''

# 模板 - 声明枚举
TEMPLATE_DECLARE_ENUM = '''
export enum {enumName} {{
	{enumFields}
}}'''

# 模板 - 声明消息
TEMPLATE_DECLARE_MESSAGE = '''
/**
* from pack `{pack}`
*/
export interface {messageName} {{
	{messageFields}
}}'''
TEMPLATE_DECLARE_MESSAGE2 = '''
/**
* from pack `{pack}`
*/
export interface {messageName} extends {parentMessage} {{
	{messageFields}
}}'''

# 模板 - JS消息
TEMPLATE_JS_MESSAGE = '''
// message '{messageName}'
pbMgr.addMessagePacker('{messageName}',{{
	pack:function(data){{
		var msg = new {messageName}();
		{packStatements}
		return msg;
	}},
	unpack:function(msg){{
		var data = {{}}
		{unpackStatements}
		return data;
	}}
}},'{pack}');'''

# 模板 - JS协议
TEMPLATE_JS_PROTOCOL = '''
// protocol '{protocol}'
pbMgr.addProtocol({protocol}, '{messageName}', {messageName}, '{pack}');'''

# JS 协议头
HEAD_JS_PROTOCOL = '''
var pbMgr = globalThis.lcc.pbMgr;
'''

def compile(sprotoPath, inPath, outPath):
	'''
		编译协议
	'''
	pack = os.path.basename(inPath)
	config = utils.getConfig()
	enums = {}
	messages = {}

	# 检查名称与命令冲突，分类结构
	names = {}
	commands = {}
	for f in os.listdir(sprotoPath):
		if not f.endswith('.json'):
			continue
		structs = py.readJson(os.path.join(sprotoPath, f))
		for s in structs:
			stype = s['type']
			if stype == 'message':
				sname = s['name']
				if sname in names:
					logging.error('proto name `%s` conflict in `%s` and `%s`' % (sname, s['source'], names[sname]['source']))
					return False
				names[sname] = s
				messages[sname] = s
				if s['export']:
					scommand = s['command']
					if scommand in commands:
						logging.error('proto command `%d` conflict in `%s` and `%s`' % (scommand, s['source'], commands[scommand]['source']))
						return False
					commands[scommand] = s
			elif stype == 'enum':
				sname = s['name']
				if sname in names:
					logging.error('proto name `%s` conflict in `%s` and `%s`' % (sname, s['source'], names[sname]['source']))
					return False
				names[sname] = s
				enums[sname] = s
	
	# 构建
	decCommands = []
	decMessages = []
	jsCommands = []
	jsEnums = []
	jsMessages = []
	jsProtocols = []
	protoMessages = []
	protoPath = os.path.join(outPath, 'compile', 'protos')
	if not os.path.exists(protoPath):
		os.makedirs(protoPath)
	for enum in enums.values():
		value = 0
		decEnumFields = []
		jsEnumFields = []
		for field in enum['fields']:
			value = field.get('value', value)
			#decEnumFields.append('/**\n\t* from pack `%s`\n\t*/\n\texport const %s:number;' % (pack,(field['name'])))
			decEnumFields.append('/**\n\t* from pack `%s`\n\t*/\n\t%s,' % (pack,(field['name'])))
			jsEnumFields.append('enumMap.%s = %d;' % (field['name'], value))
			value += 1
		decMessages.append(TEMPLATE_DECLARE_ENUM.format(
			enumName = enum['name'],
			enumFields = '\n\t'.join(decEnumFields)
		))
		jsEnums.append(TEMPLATE_JS_ENUM.format(
			enumName = enum['name'],
			enumFields = '\n'.join(jsEnumFields)
		))
	for message in messages.values():
		messageName = 'proto.%s.%s' % (pack, message['name'])
		decMessageFields = []
		protoMessageFields = []
		packStatements = []
		unpackStatements = []
		if message['export']:
			decCommands.append('/**\n* from pack `%s`\n*/\nexport const %s:number;' % (pack,message['name'].upper()))
			jsCommands.append('protocol.%s = %d;' % (message['name'].upper(), message['command']))
			jsProtocols.append(TEMPLATE_JS_PROTOCOL.format(
				protocol = message['command'],
				messageName = messageName,
				pack = pack,
			))
		if 'parent' in message:
			protoMessageFields.append('%s __super = 1;' % message['parent'])
		location = 2
		if 'parent' in message:
			unpackStatements.append('data = pbMgr.unpackMessage("proto.%s.%s", msg.getSuper());' % (pack, message['parent']))
			packStatements.append('msg.setSuper(pbMgr.packMessage("proto.%s.%s", data));'% (pack, message['parent']))
		for field in message['fields']:
			ftype = field['type']
			if 'location' in field:
				location = field['location'] + 1
			if '_group' in ftype:
				tstype = '|'.join([ t + '[]' for t in ftype['type']['ts'].split('|')])
				_p3type = ftype['type']['p3']
				if _p3type in enums:
					_p3type = 'int32'
				p3type = 'repeated ' + _p3type
				if _p3type in messages:
					unpackStatements.append('data.%s = msg.get%sList().map(function(v){ return pbMgr.unpackMessage("proto.%s.%s", v); });' % (field['name'], field['name'].capitalize(),pack,_p3type))
				else:
					unpackStatements.append('data.%s = msg.get%sList();' % (field['name'], field['name'].capitalize()))
				packStatements.append('if(data.%s != null){' % field['name'])
				packStatements.append('\tfor(var e of data.%s){' % field['name'])
				if _p3type in messages:
					packStatements.append('\t\tmsg.add%s(pbMgr.packMessage("proto.%s.%s", e))' % (field['name'].capitalize(),pack,_p3type))
				else:
					packStatements.append('\t\tmsg.add%s(e)' % field['name'].capitalize())
				packStatements.append('\t}')
				packStatements.append('}')
			elif '_map' in ftype:
				tstype = '''{[key:%s]:%s}''' % (ftype['key']['ts'], ftype['value']['ts'])
				_p3key = ftype['key']['p3']
				_p3value = ftype['value']['p3']
				if _p3key in enums:
					_p3key = 'int32'
				if _p3value in enums:
					_p3value = 'int32'
				p3type = '''map<%s,%s>''' % (_p3key, _p3value)
				unpackStatements.append('var _map = {};')
				unpackStatements.append('var map = msg.get%sMap();' % field['name'].capitalize())
				unpackStatements.append('for (var k of map.keys()){')
				unpackStatements.append('\tvar v = map.get(k);')
				if _p3value in messages:
					unpackStatements.append('\tv = pbMgr.unpackMessage("proto.%s.%s", v);' % (pack, _p3value))
				unpackStatements.append('\t_map[k] = v;')
				unpackStatements.append('}')
				unpackStatements.append('data.%s = _map;' % field['name'])
				packStatements.append('if(data.%s != null){' % field['name'])
				packStatements.append('\tvar map = msg.get%sMap();' % field['name'].capitalize())
				packStatements.append('\tfor(var k in data.%s){' % field['name'])
				packStatements.append('\t\tvar v = data.%s[k];' % field['name'])
				if _p3value in messages:
					packStatements.append('\t\tv = pbMgr.packMessage("proto.%s.%s", v);' % (pack, _p3value))
				packStatements.append('\t\tmap.set(k, v);')
				packStatements.append('\t}')
				packStatements.append('}')
			else:
				tstype = ftype['ts']
				p3type = ftype['p3']
				if p3type in enums:
					p3type = 'int32'
				if p3type in messages:
					unpackStatements.append('''data.{name} = pbMgr.unpackMessage('proto.{pack}.{message}', msg.get{name2}());'''.format(
						name = field['name'],
						name2 = field['name'].capitalize(),
						pack = pack,
						message = p3type,
					))
					packStatements.append('''if(data.{name} != null)msg.set{name2}(pbMgr.packMessage("proto.{pack}.{message}", data.{name}));'''.format(
						name = field['name'],
						name2 = field['name'].capitalize(),
						pack = pack,
						message = p3type,
					))
				else:
					unpackStatements.append('data.%s = msg.get%s();' % (field['name'], field['name'].capitalize()))
					packStatements.append('if(data.%s != null)msg.set%s(data.%s);' % (field['name'], field['name'].capitalize(), field['name']))
			if field.get('option', False):
				decMessageFields.append('%s?:%s;' % (field['name'], tstype))
			else:
				decMessageFields.append('%s:%s;' % (field['name'], tstype))
			protoMessageFields.append('%s %s = %d;' % (p3type, field['name'], location))

			location += 1
		if 'parent' in message:
			decMessages.append(TEMPLATE_DECLARE_MESSAGE2.format(
				pack = pack,
				messageName = message['name'],
				parentMessage = message['parent'],
				messageFields = '\n\t'.join(decMessageFields)
			))
		else:
			decMessages.append(TEMPLATE_DECLARE_MESSAGE.format(
				pack = pack,
				messageName = message['name'],
				messageFields = '\n\t'.join(decMessageFields)
			))
		protoMessages.append(TEMPLATE_PROTO_MESSAGE.format(
			messageName = message['name'],
			messageFields = '\n\t'.join(protoMessageFields)
		))
		jsMessages.append(TEMPLATE_JS_MESSAGE.format(
			messageName = messageName,
			packStatements = '\n\t\t'.join(packStatements),
			unpackStatements = '\n\t\t'.join(unpackStatements),
			pack = pack,
		))
	if len(decCommands) > 0 or len(decMessages) > 0:
		with open(os.path.join(protoPath, '%s.d.ts' % pack), 'w') as f:
			f.write(TEMPLATE_DECLARE_FILE.format(
				commands = '\n'.join(decCommands),
				messages = '\n'.join(decMessages),
			))
	if len(jsCommands) > 0 or len(jsEnums) > 0:
		with open(os.path.join(protoPath, '%s.js' % pack), 'w') as f:
			f.write(TEMPLATE_JS_FILE.format(
				commands = '\n'.join(jsCommands),
				enums = '\n'.join(jsEnums),
			))
	if py.getDictPath(config, 'build.protobuf', False) and len(protoMessages) > 0:
		protoFile = os.path.join(protoPath, '%s.proto' % pack)
		with open(protoFile, 'w') as f:
			f.write(TEMPLATE_PROTO_FILE.format(
				packName = pack,
				messages = '\n'.join(protoMessages),
			))
		utils.protoJS(protoFile)
		protoJSFile = os.path.join(protoPath, '%s_pb.js' % pack)
		with open(protoJSFile, 'r') as rf:
			protoContent = rf.read()
		protoContent = HEAD_JS_PROTOCOL + \
			protoContent.replace('''require('google-protobuf')''', 'globalThis.lcc.__jspb') \
						.replace('''Function('return this')()''', 'globalThis')
		protoContent = protoContent + '\n'.join(jsMessages)
		protoContent = protoContent + '\n'.join(jsProtocols)
		with open(protoJSFile, 'w') as wf:
			wf.write(protoContent)

	return True
