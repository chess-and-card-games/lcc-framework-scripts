# -*- coding: utf-8 -*-

import os,shutil,logging
from .. import config as C
from .. import utils
from ..utils import py
from .preprocess import preprocessPack
from .compile import compilePack
from .postprocess import postprocessProject

'''
	构建资源
'''
def build():
	config = utils.getConfig()
	originPath = utils.fullConfigPath(config['path']['origins'])

	# 预处理与编译包
	packs = []
	for f in os.listdir(originPath):
		if not f.startswith('_'):
			packs.append(f)
			inPath = os.path.join(originPath, f)
			if os.path.isdir(inPath):
				outPath = os.path.join(C.BUILD_CACHES_PATH, f)
				if not preprocessPack(inPath, outPath, py.getDictPath(config, 'build.force', False)):
					logging.error('preprocess pack `%s` fail' % f)
					return False
				if not compilePack(inPath, outPath):
					logging.error('compile pack `%s` fail' % f)
					return False

	# 剔除无用包
	buildPacks = [ f for f in os.listdir(C.BUILD_CACHES_PATH) if os.path.isdir(os.path.join(C.BUILD_CACHES_PATH, f)) ]
	for p in buildPacks:
		if p not in packs:
			shutil.rmtree(os.path.join(C.BUILD_CACHES_PATH, p))

	# 后处理工程
	if not postprocessProject(C.BUILD_CACHES_PATH):
		logging.error('postprocess project fail')
		return False
