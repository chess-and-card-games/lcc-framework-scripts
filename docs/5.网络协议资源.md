****
[原始资源配置与构建文档](./1.原始资源配置与构建文档.md)<br/>
****
<br/>

# **网络协议资源**

网络协议资源是框架自定义的一种通信协议定义文件。它定义了分包所使用协议的协议号和协议格式，并且还可以通过协议格式自动生成和引入`Protobuf`消息文件。

## **配置网络协议资源**

配置网络协议资源也就是定义使用的网络协议。网络协议的结构定义和`Typescript`的`interface`定义很类似，只是添加了`Protobuf`的数据类型和标识号格式。<br/>

1. ### 定义简单的网络协议
	根据如下实例讲解：<br/>
	```
		export(1000) interface ProtoTest1 {
			value1?:number;
			value2?:string[];
			value3?:{[key:number]:boolean};
			value4?:Uint8Array;
		}
	```
	上面，使用`Typescript`语法定义了`ProtoTest1`协议。<br/>
	* `export(1000)` 为导出协议关键字，这里导出的协议号为1000。<br/>
		`不用关心协议号的具体数值，只要保证不冲突不变化就可以了。`
	* `interface` 为定义协议关键字，这里也可以使用`struct`和`message`。
	* `ProtoTest1` 为协议名称，需要符合标志标识符规范。<br/>
		`注意不能全部是大写`
	* `value1` ... `value4` 为协议字段，需要符合标志标识符规范。
	* `:number` ... `:Uint8Array` 为字段类型，也可以使用部分`Protobuf`数据类型。
	* 协议使用默认的字段标识号，`value1` ... `value4` 依次为 1 ... 4。字段标识号是`Protobuf`消息需要的。

	可以使用`Protobuf`数据类型和标识号把`ProtoTest1`协议重新定义一遍。<br/>
	```
		export(1000) message ProtoTest1 {
			value1?:int32 = 1;
			value2?:string[] = 2;
			value3?:{[key:int32]:bool} = 3;
			value4?:bytes = 4;
		}
	```
	上面两种定义方式基本一样，而且可以混合使用。只要注意`Protobuf`数据类型和`Typescript`数据类型的对应方式。<br/>

	数据类型对应表如下：
	|名称|Typescript类型|Protobuf类型|
	|----|----|----|
	|整数|number|int32|
	|单精度浮点数|number|float|
	|双精度浮点数|number|double|
	|双精度浮点数|number|double|
	|布尔值|boolean|bool|
	|字符串|string|string|
	|二进制块|Uint8Array|bytes|
	|数组|数据类型[]||
	|映射表|{[key:数值或者字符串类]:数据类型}|map<数值或者字符串类,数据类型>|

	由上表可知，数组只能使用`Typescript类型`格式定义。

2. ### 定义和使用枚举
	枚举的定义和`Typescript`的枚举一样，而且它可以作为整数类型在协议中使用。例如<br/>
	```
		enum EnumTest {
			ENUM1 = 0,
			ENUM2,
			ENUM3,
		}

		export(1000) interface ProtoTest1 {
			value1?:number;
			value2?:string[];
			value3?:{[key:number]:boolean};
			value4?:Uint8Array;
			value5?:EnumTest;
		}
	```
	如上，`value5`的类型使用枚举`EnumTest`，本质是一个整数类型。<br/>
	使用方式和对应`Typescript`一致。

3. ### 引用其他的协议
	协议也可以作为数据类型，被其他协议引用。<br/>
	协议的引用有两种方式：
	* 被协议字段引用。比如如下定义；
		```
			interface ProtoTest1 {
				value1?:number;
				value2?:string[];
				value3?:{[key:number]:boolean};
				value4?:Uint8Array;
			}

			export(1001) interface ProtoTest2 {
				value1?:ProtoTest1;
			}
		```
		协议`ProtoTest2`的字段`value1`引用了协议`ProtoTest1`类型。

	* 通过继承引用。比如如下定义；
		```
			export(1002) interface ProtoTest3 extends ProtoTest1 {
				value1?:number;
			}
		```
		协议`ProtoTest3`通过继承引用了协议`ProtoTest1`类型。
	
	如果协议只作为类型使用，那么可以不用`export()`导出。<br/>
	使用方式和对应`Typescript`一致。

## **使用构建生成的网络协议**

网络协议资源构建后会生成对应的`.js`协议代码和`.d.ts`声明文件。<br/>
在构建后，协议文件中的`枚举`、`协议`会直接导入到`lcc.protocol`名称空间中。
同时，对应`协议`的`协议号`也会以常量的方式导入到`lcc.protocol`名称空间中，常量名称就是协议名称全大写。<br/>
如下实例，协议文件如下：<br/>
```
	enum EnumTest {
		ENUM1 = 0,
		ENUM2,
		ENUM3,
	}
	export(1000) interface ProtoTest1 {
		value1?:number;
		value2?:string[];
		value3?:{[key:number]:boolean};
		value4?:Uint8Array;
		value5?:EnumTest;
	}
	export(1001) interface ProtoTest2 {
		value1?:ProtoTest1;
	}
	export(1002) interface ProtoTest3 extends ProtoTest1 {
		value1?:number;
	}
```
构建后生成的声明文件如下：<br/>
```
	declare namespace lcc {
		export namespace protocol {
			export const PROTOTEST1:number;
			export const PROTOTEST2:number;
			export const PROTOTEST3:number;

			export enum EnumTest {
				ENUM1,
				ENUM2,
				ENUM3,
			}
			export interface ProtoTest1 {
				value1?:number;
				value2?:string[];
				value3?:{[key:number]:boolean};
				value4?:Uint8Array|string;
				value5?:EnumTest;
			}
			export interface ProtoTest2 {
				value1?:ProtoTest1;
			}
			export interface ProtoTest3 extends ProtoTest1 {
				value1?:number;
			}
		}
	}
```



